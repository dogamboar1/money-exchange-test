
import React from 'react'
import History from './../components/History/History'

function HistoryPage(props) {
    return (
        <History {...props} />
    )
}

export default HistoryPage
