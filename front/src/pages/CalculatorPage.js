import React from 'react'
import Calculator from '../components/Calculator/Calculator'


function CalculatorPage(props) {
    return (
        <Calculator {...props} />
    )
}

export default CalculatorPage
