
import React from 'react'

function PageError(props) {
    return (
        <h1>
            404 Page not found
        </h1>
    )
}

export default PageError
