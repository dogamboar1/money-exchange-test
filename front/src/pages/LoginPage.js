import React, { Component } from 'react';

class LoginPage extends Component {

    render() {

        const {
            user,
            signOut,
            signInWithGoogle,
        } = this.props;
        
        if (user) {
            return (
                <React.Fragment>
                    <p>Hello, {user.displayName}</p>
                    <button onClick={signOut}>Sign out</button>
                </React.Fragment>
            )
        } else {
            return (
                <button onClick={signInWithGoogle}>Sign in with Google</button>
            )
        }
    }
}

export default LoginPage