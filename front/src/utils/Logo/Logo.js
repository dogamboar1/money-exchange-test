import React from 'react'
import './Logo.css'

function Logo() {
    return (
        <div className='logo-container'>
            <div className='logo sphere' />
        </div>
    )
}

export default Logo
