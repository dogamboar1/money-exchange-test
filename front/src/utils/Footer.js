import React from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import TextAreaSkeleton from './Skeletons/TextAreaSkeleton'

function Footer() {
    return (
        <Container className='footer'>
            <Row>
                <Col xs={12} lg={4} md={4}><TextAreaSkeleton /></Col>
                <Col xs={12} lg={4} md={4}><TextAreaSkeleton /></Col>
                <Col xs={12} lg={4} md={4}><TextAreaSkeleton /></Col>
            </Row>
        </Container>
    )
}

export default Footer
