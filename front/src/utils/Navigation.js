import React from 'react'
import Nav from 'react-bootstrap/Nav'
import { Link} from "react-router-dom";

const Navigation = (props) => {

    if (props.user) {
        return(
            <Nav className="justify-content-center" activeKey="/home">
                <Nav.Item>
                    <Link to="/home" className='nav-link'>Calculator</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/history" className='nav-link'>History</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/about_us" className='nav-link'>About us</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/account" className='nav-link'>Account</Link>
                </Nav.Item>
            </Nav>
        )
    } else {
        return(
            <Nav className="justify-content-center" activeKey="/home">
                <Nav.Item>
                    <Link to="/home" className='nav-link'>Calculator</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/about_us" className='nav-link'>About us</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to="/account" className='nav-link'>Account</Link>
                </Nav.Item>
            </Nav>
        )
    }
}

export default Navigation
