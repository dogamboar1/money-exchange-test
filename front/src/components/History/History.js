import React, { Component } from 'react'
import { db } from './../../utils/firebase'
import Table from 'react-bootstrap/Table'

class History extends Component {

    constructor(props) {
        super(props);
        this.state = {
          convertions: []
        };
    }

    componentDidMount() {

        if (this.props.user) {

            const uid = this.props.user.uid;

            db.collection("exchanges")
                .doc(uid)
                .collection('history')
                .get()
                .then(querySnapshot => {
                    const data = querySnapshot.docs.map(doc => doc.data());
                    console.log(data);
                    this.setState({ convertions: data });
                });
        }
        
    }

    render() {
        console.log('this.props', this.props)
        const { convertions } = this.state;

        if (convertions.length > 0){
            return (
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>From</th>
                        <th>To</th>
                        </tr>
                    </thead>
                    <tbody>
                    {convertions.map((convertion, index) => {
                        const date = new Date(convertion.timestamp).toUTCString()
                        return (
                            <tr>
                                <td>{index}</td>
                                <td>{date}</td>
                                <td>{`(${convertion.from}) ${convertion.value}`}</td>
                                <td>{`(${convertion.to}) ${convertion.converted_value}`}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            )
        } else {
            return (
                <h3>Without records</h3>
            )
        }
        
    }
}

export default History
