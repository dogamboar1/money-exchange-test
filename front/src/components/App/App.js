import React, { Component } from 'react';
import withFirebaseAuth from 'react-with-firebase-auth'
import { firebaseAppAuth, providers } from './../../utils/firebase';
import Routes from './Routes'

import 'bootstrap/dist/css/bootstrap.min.css';
import './../../assets/styles/styles.scss'

import Logo from '../../utils/Logo/Logo'
import Footer from './../../utils/Footer'

class App extends Component {

    render() {
        console.log(this.props.user)
        return (
            <React.Fragment>
                <Logo />
                <Routes {...this.props} />
                <Footer />
            </React.Fragment>
        );
    }
}

export default withFirebaseAuth({
    providers,
    firebaseAppAuth,
})(App);
