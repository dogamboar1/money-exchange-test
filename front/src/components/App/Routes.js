import React, { Component } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import CalculatorPage from '../../pages/CalculatorPage'
import PageError from '../../pages/PageError'
import LoginPage from '../../pages/LoginPage'
import HistoryPage from './../../pages/HistoryPage'
import Navigation from './../../utils/Navigation'


export class Routes extends Component {

    render() {

        if (this.props.user) {
            return (
                <BrowserRouter>
                    <Navigation {...this.props} />
                    <div className='page'>
                        <Redirect
                            from="/"
                            to="/home" />
                        <Switch>
                            <Route
                                path="/home"
                                render={(props) => <CalculatorPage {...this.props} />} 
                            />
                            <Route
                                path="/history"
                                render={(props) => <HistoryPage {...this.props} />} 
                            />
                            <Route
                                path='/account'
                                render={(props) => <LoginPage {...this.props} />}
                            />
                            <Route component={PageError} />
                        </Switch>
                    </div>
                </BrowserRouter>
            )
        } else {
            return (
                <BrowserRouter>
                    <Navigation {...this.props} />
                    <div className='page'>
                        <Redirect
                            from="/"
                            to="/home" />
                        <Switch>
                            <Route
                                path="/home"
                                render={(props) => <CalculatorPage {...this.props} />} 
                            />
                            <Route
                                path='/account'
                                render={(props) => <LoginPage {...this.props} />}
                            />
                            <Route component={PageError} />
                        </Switch>
                    </div>
                </BrowserRouter>
            )
        }
        
    }
}

export default Routes
