import React from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import './Calculator.scss'
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import Alert from 'react-bootstrap/Alert'
import { db } from './../../utils/firebase'


let timer

const numberMaskUsd = createNumberMask({
    prefix: '$',
    suffix: '', // This will put the dollar sign at the end, with a space.
    allowDecimal: true,
    decimalSymbol: '.',
    decimalLimit: 4,
    requireDecimal: false,
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ',',
})

const numberMaskEuro = createNumberMask({
    prefix: '€',
    suffix: '', // This will put the dollar sign at the end, with a space.
    allowDecimal: true,
    decimalSymbol: '.',
    decimalLimit: 4,
    requireDecimal: false,
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ',',
})

class Calculator extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            from: 'USD',
            to: 'EUR',
            value: '',
            converted_value: 0,
            error: '',
        }
    }

    handleChange = (event) => {
        this.setState({ value: event.target.value, converted_value: '' });
    }

    handleSubmit = (event) => {
        event.preventDefault()
        var value = Number(this.state.value.replace(/[^0-9.-]+/g, ""));
        if (value !== '') {
            this.fetchConvertion(this.state.from, this.state.to, value)
        }
    }

    componentWillUnmount() {
        clearTimeout(timer);
    }

    setConversion = (conversion) => {
        if (conversion.code === 200) {
            this.setState({ converted_value: conversion.converted_value, error: '' })
            timer = setTimeout(this.fetchConvertion, 1000 * 60 * 10, conversion.from, conversion.to, conversion.value);
            this.saveConversion()
        }
    }

    saveConversion = () => {

        const uid = ( this.props.user ) ? this.props.user.uid : 'anonymous';
        const current_time = new Date().getTime()

        const data = {
            ...this.state,
            timestamp: current_time,
        };
        
        db.collection("exchanges")
        .doc(uid)
        .collection('history')
        .doc(`${current_time}`)
        .set(data)
        .then(() => {
            
        })
        .catch(error => {
            this.setState({ error: `Error save` })
        });
    }

    fetchConvertion = (from, to, value) => {
        let self = this
        fetch(`http://localhost:8080/v1/exchange/${from}/${to}/${value}`)
            .then(function (response) {
                if (response.ok) {
                    response.json().then(self.setConversion);
                }
            })
            .catch(function (error) {
                console.error(error.message)
                self.setState({ error: `We are currently out of service, try later` })
            });
    }

    render() {
        const submitIsDisabled = (this.state.value !== '') ? false : true
        return (
            <Container className='calculator-container'>
                {this.state.error !== '' &&
                    <Alert key='1' variant='danger'>{this.state.error}</Alert>
                }
                <form onSubmit={this.handleSubmit}>
                    <Row>
                        <Col>
                            <label>
                                <span>{this.state.from}</span>
                                <MaskedInput
                                    className='form-control'
                                    mask={numberMaskUsd}
                                    onChange={this.handleChange}
                                />
                            </label>
                        </Col>
                        <Col>
                            <label>
                                <span>{this.state.to}</span>
                                <MaskedInput
                                    className='form-control'
                                    mask={numberMaskEuro}
                                    value={this.state.converted_value}
                                    disabled
                                />
                            </label>
                        </Col>
                    </Row>
                    <Row>
                        <Col><input type="submit" value="Convert" className='btn btn-primary btn-lg' disabled={submitIsDisabled} /></Col>
                    </Row>
                </form>
            </Container>
        )
    }
}

export default Calculator