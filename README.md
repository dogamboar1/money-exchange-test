# Money Exchange App

## Overview
This project contains two components, a node server that exposes a rest resource and a UI component.

### Running the server
To run the server, run:

```
cd nodejs-server-server
node index.js
```

### Running the front UI

To run the front UI, run:

```
cd front
npm i
npm start
```