'use strict';

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
/**
 * Get the conversion of a source currency to a destination currency
 *
 * from String Currency source
 * to String Currency target
 * value BigDecimal Currency value
 * returns ExchangeResponse
 **/
exports.getConversion = function(from,to,value) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : 200,
  "source" : from,
  "expiry" : 1.46581298050294517310021547018550336360931396484375,
  "converted_value" : getRandomArbitrary(1,2) * value,
  "value" : value,
  "target" : to
};
    if (Object.keys(examples).length > 0) { 
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

