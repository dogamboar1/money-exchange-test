'use strict';

var utils = require('../utils/writer.js');
var Echange = require('../service/EchangeService');

module.exports.getConversion = function getConversion (req, res, next) {
  var from = req.swagger.params['from'].value;
  var to = req.swagger.params['to'].value;
  var value = req.swagger.params['value'].value;
  Echange.getConversion(from,to,value)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
